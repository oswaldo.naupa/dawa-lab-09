const express = require("express");
const bcrypt = require("bcrypt");
const Usuario = require("../models/usuario");

const {verificaToken} = require("../middlewares/authentication")

const app = express();

app.post("/usuario", function(req, res){
    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role,
    });

    usuario.save((err, usuarioDB) => {
        if(err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }

        usuarioDB.password =  null;

        res.json({
            ok: true,
            usuario: usuarioDB,
        });
    });
});

app.get("/usuario",verificaToken, (req, res) => {
    //res.setHeader("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7InJvbGUiOiJVU0VSX1JPTEUiLCJlc3RhZG8iOnRydWUsIl9pZCI6IjVlZDgyOGQ1YTYxMWI0NTk0YzY4N2IzOSIsIm5vbWJyZSI6Im9zd2FsZG8iLCJlbWFpbCI6Im9zd2FsZG8xMjM2NTQ1QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiJDJiJDEwJG9lcFlaVDZFTXFQcC85eXQubWhyYWVYaW5jZWFSWDhuRHd1QkgzUE1YMy8yVElVQW9DNUg2IiwiX192IjowfSwiaWF0IjoxNTkxOTMyMjc3LCJleHAiOjE1OTE5MzQ4Njl9.vGmJHjuseGL3Bu3A_xCKBNfPf9b-rJnZCPxT1ETYfc0");
    Usuario.find({}).exec((err, usuarios) =>{
        if(err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            usuarios,
        });
    });
});

app.put("/usuario/:id", function(req, res){
    let id = req.params.id;

    let body = req.body;

    Usuario.findByIdAndUpdate(id, body, { new: true}, (err, usuarioDB) =>{
        if(err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            usuario: usuarioDB,
        });
    });
});

app.delete("/usuario/:id", function(req, res) {
    let id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if(err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            usuario: usuarioBorrado,
        });
    });
});

module.exports = app;